package ordenadores;

/**
 * Enum que lista los tipos de ordenamiento que se pueden realizar.
 * @author Christian
 *
 */
public enum Ordenamientos 
{
	SELECCION,
	INSERCION,
	SHELL,
	QUICK,
	MERGE
}