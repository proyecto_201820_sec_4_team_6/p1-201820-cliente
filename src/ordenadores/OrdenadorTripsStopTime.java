package ordenadores;

import model.vo.Trip;


public class OrdenadorTripsStopTime {
	/**
	 * Ordena los elementos de la lista que llega por par�metro
	 * 
	 * @param algoritmo
	 *            el algoritmo que se desea usar para ordenar. algoritmo != null
	 * @param ascendente
	 *            la direcci�n del ordenamiento. true si es ascedente o false si
	 *            es descendente
	 * @param elementos
	 *            la lista de elementos que se desea ordenar. elementos != null
	 */
	public void ordenar(Ordenamientos algoritmo, boolean ascendente, Trip[] elementos) {
		switch (algoritmo)
		{
			case SELECCION:
				ordenarSeleccion(elementos, ascendente);
				break;
			case INSERCION:
				ordenarInsercion(elementos, ascendente);
				break;
			case SHELL:
				ordenarShell(elementos);
				break;
			case QUICK:
				ordenarQuick(elementos, 0, elementos.length - 1);
				break;
			case MERGE:
				Trip[] aux = new Trip [elementos.length];
				ordenarMerge(elementos, aux, 0, elementos.length-1);
				break;
		}

	}

	/**
	 * Ordena la lista usando el algoritmo de inscerci�n post: la lista se
	 * encuentra ordenada
	 * 
	 * @param lista
	 *            la lista que se desea ordenar. lsita != null
	 * @param ascendnte
	 *            indica si se debe ordenar de mamenra ascendente, de lo
	 *            contrario se ordenar� de manera descendente
	 * @param comparador
	 *            comparador de elementos tipo T que se usar� para ordenar la
	 *            lista, define el criterio de orden. comparador != null.
	 */
	private void ordenarInsercion(Trip[] lista, boolean ascendente) 
	{
		if (ascendente)
		{
			int i = 1;
			while (i < lista.length)
			{
				int j = i;
				while (j > 0 && (lista[j].compareToStopTime(lista[j-1]) == 1))
					
				{
					Trip temp = lista[j-1];
					lista[j-1] = lista[j];
					lista[j] = temp;
					j--;
				}
				i++;
			}
		}
		else
		{
			int i = 1;
			while (i < lista.length)
			{
				int j = i;
				while (j > 0 && (lista[j].compareToStopTime(lista[j-1]) == -1))
				{
					Trip temp = lista[j-1];
					lista[j-1] = lista[j];
					lista[j] = temp;
					j--;
				}
				i++;
			}
		}
	}

	/**
	 * Ordena la lista usando el algoritmo de selecci�n post: la lista se
	 * encuentra ordenada
	 * 
	 * @param lista
	 *            la lista que se desea ordenar. lsita != null
	 * @param ascendnte
	 *            indica si se debe ordenar de mamenra ascendente, de lo
	 *            contrario se ordenar� de manera descendente
	 * @param comparador
	 *            comparador de elementos tipo T que se usar� para ordenar la
	 *            lista, define el criterio de orden. comparador != null.
	 */
	private void ordenarSeleccion(Trip[] lista, boolean ascendente) 
	{
		{
			if(ascendente)
			{
				for( int i = 0; i < lista.length - 1; i++ )
				{
					Trip menor = lista[i];
					int menorPosicion= i;
					for( int j = i + 1; j < lista.length; j++ )
					{
						if( lista[j].compareToStopTime(menor) == -1)
						{
							menor = lista[j];
							menorPosicion = j;
						}
					}
					Trip temp = lista[i];
					lista[i] = menor;
					lista[menorPosicion] = temp;
				}
			}
			else
			{
				for( int i = 0; i < lista.length - 1; i++ )
				{
					Trip mayor = lista[i];
					int menorPosicion= i;
					for( int j = i + 1; j < lista.length; j++ )
					{
						if( lista[j].compareToStopTime(mayor) == 1)
						{
							mayor = lista[j];
							menorPosicion = j;
						}
					}
					Trip temp = lista[i];
					lista[i] = mayor;
					lista[menorPosicion] = temp;
				}
			}
		}
	}
	
	/**
     * Rearranges the array in ascending order, using the natural order.
     * @param a the array to be sorted
     */
    private void ordenarShell(Trip[] a) {
        int n = a.length;
        // 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
        int h = 1;
        while (h < n/3) h = 3*h + 1; 

        while (h >= 1) {
            // h-sort the array
            for (int i = h; i < n; i++) {
                for (int j = i; j >= h && a[j].compareToStopTime(a[j-h]) == -1 ; j -= h) {
                	Trip swap = a[j];
                    a[j] = a[j-h];
                    a[j-h] = swap;                }
            }
            assert isHsorted(a, h); 
            h /= 3;
        }
        assert isSorted(a);
    }
    
    //para el shell
    private static boolean isHsorted(Trip[] a, int h) {
        for (int i = h; i < a.length; i++)
            if (a[i].compareToStopTime(a[i-h]) == -1) return false;
        return true;
    }
    
    private void ordenarQuick(Trip[] lista, int inicio, int fin)
    {
    	if(inicio >= fin)
    	{
    		return;
    	}
    	int i = inicio;
        int j = fin;
        int piv = (i + j)/2;
        if (piv <= i || piv >= j)
        {
        	return;
        }
        Trip pivot = lista[piv];
        while(lista[i].compareToStopTime(pivot) == -1)
        {
        	i++;
        }
        while(lista[j].compareToStopTime(pivot) == 1)
        {
        	j--;
        }
        if (lista[i].compareToStopTime(lista[j]) >= 0)
        {
        	Trip temp = lista[i];
        	lista[i] = lista[j];
        	lista[j] = temp;
        	i++;
        	j--;
        }
        ordenarQuick(lista, inicio, piv);
        ordenarQuick(lista, piv, fin);
        return;
    }
    
    public boolean isSorted(Trip[] a) {
        for (int i = 1; i < a.length; i++)
            if (a[i].compareToStopTime(a[i-1]) == -1) return false;
        return true;
    }
    
    private void merge(Trip[] a, Trip[] aux, int lo, int mid, int hi) {
		// TODO Auto-generated method stub
    	assert isSorted(a, lo, mid);
        assert isSorted(a, mid+1, hi);

        // copy to aux[]
        for (int k = lo; k <= hi; k++) {
            aux[k] = a[k]; 
        }

        // merge back to a[]
        int i = lo, j = mid+1;
        for (int k = lo; k <= hi; k++) {
            if      (i > mid)              a[k] = aux[j++];
            else if (j > hi)               a[k] = aux[i++];
            else if (aux[j].compareToStopTime(aux[i]) == -1 ) a[k] = aux[j++];
            else                           a[k] = aux[i++];
        }

        // postcondition: a[lo .. hi] is sorted
        assert isSorted(a, lo, hi);
	}
    
    private void ordenarMerge(Trip[] a, Trip[] aux, int lo, int hi) {
        if (hi <= lo) return;
        int mid = lo + (hi - lo) / 2;
        ordenarMerge(a, aux, lo, mid);
        ordenarMerge(a, aux, mid + 1, hi);
        merge(a, aux, lo, mid, hi);
    }
    
    private static boolean isSorted(Trip[] a, int lo, int hi) {
        for (int i = lo + 1; i <= hi; i++)
            if (a[i].compareToStopTime(a[i-1]) == -1) return false;
        return true;
    }
}