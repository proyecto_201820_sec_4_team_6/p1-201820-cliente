package ordenadores;

import java.time.LocalDateTime;

import model.vo.Station;


public class OrdenadorStations {
	/**
	 * Ordena los elementos de la lista que llega por par�metro
	 * 
	 * @param algoritmo
	 *            el algoritmo que se desea usar para ordenar. algoritmo != null
	 * @param ascendente
	 *            la direcci�n del ordenamiento. true si es ascedente o false si
	 *            es descendente
	 * @param comparador
	 *            el criterio de comparaci�n que se usar�. comaprador != null
	 * @param elementos
	 *            la lista de elementos que se desea ordenar. elementos != null
	 */
	public void ordenar(Ordenamientos algoritmo, boolean ascendente, Station[] elementos) {
		switch (algoritmo)
		{
			case SELECCION:
				ordenarSeleccion(elementos, ascendente);
				break;
			case INSERCION:
				ordenarInsercion(elementos, ascendente);
				break;
			case SHELL:
				ordenarShell(elementos);
				break;
			case QUICK:
				ordenarQuick(elementos, 0, elementos.length - 1);
				break;
			case MERGE:
				Station[] aux = new Station [elementos.length];
				ordenarMerge(elementos, aux, 0, elementos.length-1);
				break;
		}

	}

	/**
	 * Ordena la lista usando el algoritmo de inscerci�n post: la lista se
	 * encuentra ordenada
	 * 
	 * @param elementos
	 *            la lista que se desea ordenar. lsita != null
	 * @param ascendnte
	 *            indica si se debe ordenar de mamenra ascendente, de lo
	 *            contrario se ordenar� de manera descendente
	 * @param comparador
	 *            comparador de elementos tipo T que se usar� para ordenar la
	 *            lista, define el criterio de orden. comparador != null.
	 */
	private void ordenarInsercion(Station[] elementos, boolean ascendente) 
	{
		if (ascendente)
		{
			int i = 1;
			while (i < elementos.length)
			{
				int j = i;
				while (j > 0 && (elementos[j].compareTo(elementos[j-1]) == 1))
					
				{
					Station temp = elementos[j-1];
					elementos[j-1] = elementos[j];
					elementos[j] = temp;
					j--;
				}
				i++;
			}
		}
		else
		{
			int i = 1;
			while (i < elementos.length)
			{
				int j = i;
				while (j > 0 && (elementos[j].compareTo(elementos[j-1]) == -1))
				{
					Station temp = elementos[j-1];
					elementos[j-1] = elementos[j];
					elementos[j] = temp;
					j--;
				}
				i++;
			}
		}
	}

	/**
	 * Ordena la lista usando el algoritmo de selecci�n post: la lista se
	 * encuentra ordenada
	 * 
	 * @param elementos
	 *            la lista que se desea ordenar. lsita != null
	 * @param ascendnte
	 *            indica si se debe ordenar de mamenra ascendente, de lo
	 *            contrario se ordenar� de manera descendente
	 * @param comparador
	 *            comparador de elementos tipo T que se usar� para ordenar la
	 *            lista, define el criterio de orden. comparador != null.
	 */
	private void ordenarSeleccion(Station[] elementos, boolean ascendente) 
	{
		{
			if(ascendente)
			{
				for( int i = 0; i < elementos.length - 1; i++ )
				{
					Station menor = elementos[i];
					int menorPosicion= i;
					for( int j = i + 1; j < elementos.length; j++ )
					{
						if( elementos[j].compareTo(menor) == -1)
						{
							menor = elementos[j];
							menorPosicion = j;
						}
					}
					Station temp = elementos[i];
					elementos[i] = menor;
					elementos[menorPosicion] = temp;
				}
			}
			else
			{
				for( int i = 0; i < elementos.length - 1; i++ )
				{
					Station mayor = elementos[i];
					int menorPosicion= i;
					for( int j = i + 1; j < elementos.length; j++ )
					{
						if( elementos[j].compareTo(mayor) == 1)
						{
							mayor = elementos[j];
							menorPosicion = j;
						}
					}
					Station temp = elementos[i];
					elementos[i] = mayor;
					elementos[menorPosicion] = temp;
				}
			}
		}
	}
	
	/**
     * Rearranges the array in ascending order, using the natural order.
     * @param elementos the array to be sorted
     */
    private void ordenarShell(Station[] elementos) {
        int n = elementos.length;
        // 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
        int h = 1;
        while (h < n/3) h = 3*h + 1; 

        while (h >= 1) {
            // h-sort the array
            for (int i = h; i < n; i++) {
                for (int j = i; j >= h && elementos[j].compareTo(elementos[j-h]) == -1 ; j -= h) {
                	Station swap = elementos[j];
                    elementos[j] = elementos[j-h];
                    elementos[j-h] = swap;                }
            }
            assert isHsorted(elementos, h); 
            h /= 3;
        }
        assert isSorted(elementos);
    }
    
    //para el shell
    private static boolean isHsorted(Station[] elementos, int h) {
        for (int i = h; i < elementos.length; i++)
            if (elementos[i].compareTo(elementos[i-h]) == -1) return false;
        return true;
    }
    
    private void ordenarQuick(Station[] elementos, int inicio, int fin)
    {
    	if(inicio >= fin)
    	{
    		return;
    	}
    	int i = inicio;
        int j = fin;
        int piv = (i + j)/2;
        if (piv <= i || piv >= j)
        {
        	return;
        }
        Station pivot = elementos[piv];
        while(elementos[i].compareTo(pivot) == -1)
        {
        	i++;
        }
        while(elementos[j].compareTo(pivot) == 1)
        {
        	j--;
        }
        if (elementos[i].compareTo(elementos[j]) >= 0)
        {
        	Station temp = elementos[i];
        	elementos[i] = elementos[j];
        	elementos[j] = temp;
        	i++;
        	j--;
        }
        ordenarQuick(elementos, inicio, piv);
        ordenarQuick(elementos, piv, fin);
        return;
    }
    
    public boolean isSorted(Station[] elementos) {
        for (int i = 1; i < elementos.length; i++)
            if (elementos[i].compareTo(elementos[i-1]) == -1) return false;
        return true;
    }
    
    private void merge(Station[] elementos, Station[] aux, int lo, int mid, int hi) {
    	assert isSorted(elementos, lo, mid);
        assert isSorted(elementos, mid+1, hi);

        // copy to aux[]
        for (int k = lo; k <= hi; k++) {
            aux[k] = elementos[k]; 
        }

        // merge back to a[]
        int i = lo, j = mid+1;
        for (int k = lo; k <= hi; k++) {
            if      (i > mid)              elementos[k] = aux[j++];
            else if (j > hi)               elementos[k] = aux[i++];
            else if (aux[j].compareTo(aux[i]) == -1 ) elementos[k] = aux[j++];
            else                           elementos[k] = aux[i++];
        }

        // postcondition: a[lo .. hi] is sorted
        assert isSorted(elementos, lo, hi);
	}
    
    private void ordenarMerge(Station[] elementos, Station[] aux, int lo, int hi) {
        if (hi <= lo) return;
        int mid = lo + (hi - lo) / 2;
        ordenarMerge(elementos, aux, lo, mid);
        ordenarMerge(elementos, aux, mid + 1, hi);
        merge(elementos, aux, lo, mid, hi);
    }
    
    private static boolean isSorted(Station[] elementos, int lo, int hi) {
        for (int i = lo + 1; i <= hi; i++)
            if (elementos[i].compareTo(elementos[i-1]) == -1) return false;
        return true;
    }
}