package model.vo;

import java.time.LocalDateTime;

public class Station implements Comparable<Station> {
	private int stationId;
	private String stationName;
	private String stationCity;
	private double latitude;
	private double longitude;
	private int dpcapacity;
	private LocalDateTime startDate;

	public Station(int stationId, String stationName, String stationCity, double latitude, double longitude, int dpcapacity, LocalDateTime startDate) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.setStationCity(stationCity);
		this.setDpcapacity(dpcapacity);
		this.setLatitude(latitude);
		this.setLongitude(longitude);
		this.startDate = startDate;
	}

	@Override
	//hacer esto TODO
	public int compareTo(Station that)
	{
		if( this.startDate.isBefore(that.startDate) ) return -1;
		
		else if( this.startDate.isAfter(that.startDate) ) return 1;
		
		else
		{	
			return 0;
		}
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public int getStationId() {
		return stationId;
	}

	public String getStationName() {
		return stationName;
	}

	/**
	 * @return the stationCity
	 */
	public String getStationCity() {
		return stationCity;
	}

	/**
	 * @param stationCity the stationCity to set
	 */
	public void setStationCity(String stationCity) {
		this.stationCity = stationCity;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the dpcapacity
	 */
	public int getDpcapacity() {
		return dpcapacity;
	}

	/**
	 * @param dpcapacity the dpcapacity to set
	 */
	public void setDpcapacity(int dpcapacity) {
		this.dpcapacity = dpcapacity;
	}
}
