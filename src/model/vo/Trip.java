package model.vo;

import java.time.LocalDateTime;

public class Trip implements Comparable<Trip> {

    public final static String MALE = "male";
    public final static String FEMALE = "female";
    public final static String UNKNOWN = "unknown";

    private int tripId;
    private LocalDateTime startTime;
    private LocalDateTime stopTime;
    
    private int bikeId;
    private int tripDuration;
    private int startStationId;
    private String startStation;
    private int endStationId;
    private String endStation;
    private String gender;

    public Trip(int tripId, LocalDateTime startTime, LocalDateTime stopTime, int bikeId, int tripDuration, int startStationId, String startStation, int endStationId, String endStation, String gender) {
        this.tripId = tripId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.bikeId = bikeId;
        this.tripDuration = tripDuration;
        this.startStationId = startStationId;
        this.startStation = startStation;
        this.endStationId = endStationId;
        this.endStation = endStation;
        this.gender = gender;
    }

    @Override
    public int compareTo(Trip that) {
		if( this.bikeId < that.bikeId ) return -1;
		
		else if( this.bikeId > that.bikeId ) return 1;
		
		else{
			if( this.startTime.compareTo(that.startTime) < 0 ) return -1;
			
			else if( this.startTime.compareTo(that.startTime) > 0 ) return 1;
			
			return 0;
		}
	}
    
    public int compareToFechasSolo(Trip that) {
    	if( this.startTime.compareTo(that.startTime) < 0 ) return -1;
			
		else if( this.startTime.compareTo(that.startTime) > 0 ) return 1;
			
		return 0;
	}
    
    public int compareToStopTime(Trip that) {
    	if( this.stopTime.compareTo(that.stopTime) < 0 ) return -1;
			
		else if( this.stopTime.compareTo(that.stopTime) > 0 ) return 1;
			
		return 0;
	}

    public int getTripId() {
        return tripId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getStopTime() {
        return stopTime;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTripDuration() {
        return tripDuration;
    }

    public int getStartStationId() {
        return startStationId;
    }

    public int getEndStationId() {
        return endStationId;
    }

    public String getGender() {
        return gender;
    }
    
}
