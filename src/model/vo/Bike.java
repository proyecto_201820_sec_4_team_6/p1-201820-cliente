package model.vo;

import java.util.Iterator;

public class Bike implements Comparable<Bike>{

    private int bikeId;
    private int totalTrips;
    private int totalDistance;
    private int totalDuration;

    public Bike(int bikeId, int totalTrips, int totalDistance, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = totalDistance;
        this.totalDuration = ptotalDuration;
    }

    @Override
    //TODO CHECK puede que toque hacer otro compare para duraci�n. Requisito 3C
    public int compareTo(Bike that) 
    {
    	if( this.totalDistance < that.totalDistance ) return -1;
		
		else if( this.totalDistance > that.totalDistance ) return 1;
		
		else
		{
			return 0;
		}
    }
    
    public int compareToDurationInverse(Bike that) 
    {
    	if( this.totalDuration > that.totalDuration ) return -1;
		
		else if( this.totalDuration < that.totalDuration ) return 1;
		
		else
		{
			return 0;
		}
    }
    
    public int compareToTotalTripsInverse(Bike that) 
    {
    	if( this.totalTrips > that.totalTrips ) return -1;
		
		else if( this.totalTrips < that.totalTrips ) return 1;
		
		else
		{
			return 0;
		}
    }


    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public int getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }
    
    public void addTrip()
    {
    	totalTrips ++;
    }
    
    public void addDistance(int distancia)
    {
    	totalDistance += distancia;
    }
    
    public void addDuration(int duracion)
    {
    	totalDuration += duracion;
    }
}
