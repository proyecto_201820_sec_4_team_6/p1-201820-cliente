package model.logic;

import API.IManager;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IQueue;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import ordenadores.Ordenador;
import ordenadores.OrdenadorBikes;
import ordenadores.OrdenadorBikesDuration;
import ordenadores.OrdenadorBikesTotalTrips;
import ordenadores.OrdenadorTripsFechas;
import ordenadores.OrdenadorTripsStopTime;
import ordenadores.Ordenamientos;
import tools.Harvesine;

import com.opencsv.CSVReader;

public class Manager implements IManager {
	//Ruta del archivo de datos 2017-Q1
	public static final String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	public static final String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	public static final String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	public static final String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";

	private DoublyLinkedList<Trip> listaTrips;

	private DoublyLinkedList<Station> listaStations;

	public Manager() {
		listaTrips = new DoublyLinkedList<Trip>();
		listaStations = new DoublyLinkedList<Station>();
	}

	public IQueue<Trip> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Queue<Trip> cola = new Queue<Trip>();
		Iterator<Trip> iter = listaTrips.iterator();
		while( iter.hasNext() )
		{
			Trip actual = iter.next();
			if( !(actual.getStartTime().isBefore(fechaInicial) || actual.getStopTime().isAfter(fechaFinal)) )
			{
				cola.enqueue(actual);
			}
		}
		Trip[] trips = new Trip[cola.size()];
		iter = cola.iterator();
		int i = 0;
		while (iter.hasNext() && i < trips.length)
		{
			Trip add = iter.next();
			trips[i] = add;
			i++;
		}
		OrdenadorTripsFechas order = new OrdenadorTripsFechas();
		order.ordenar(Ordenamientos.SHELL, true, trips);
		cola = new Queue<Trip>();
		i = 0;
		while (i < trips.length)
		{
			cola.enqueue(trips[i]);
			i++;
		}
		return cola;
	}

	public IDoublyLinkedList<Bike> A2BicicletasOrdenadasPorNumeroViajes( LocalDateTime fechaInicial, LocalDateTime fechaFinal)
	{
		{
			DoublyLinkedList<Bike> bicicletas = new DoublyLinkedList<Bike>();
			Iterator<Trip> iterTrips = listaTrips.iterator();
			while (iterTrips.hasNext())
			{
				Trip check = iterTrips.next();
				if(check.getStartTime().isBefore(fechaInicial) || check.getStopTime().isAfter(fechaFinal))
				{

				}
				else
				{
					Station desde = null;
					Station hasta = null;
					boolean encontroDesde = false;
					boolean encontroHasta = false;
					Iterator<Station> iterStations = listaStations.iterator();
					while (iterStations.hasNext() && (!encontroDesde || !encontroHasta) )
					{
						Station checkStation = iterStations.next();
						if (check.getStartStationId() == checkStation.getStationId())
						{
							desde = checkStation;
							encontroDesde = true;
						}
						//TODO CHECK pondr�a un else if pero que tal que est� mal y sea la misma? dunno
						//muy duro saber :(
						if (check.getEndStationId() == checkStation.getStationId())
						{
							hasta = checkStation;
							encontroHasta = true;
						}
					}
					if(desde != null || hasta != null)
					{
						int distancia = (int) Harvesine.distance(desde.getLatitude(), desde.getLongitude(), hasta.getLatitude(), hasta.getLongitude());
						Bike nueva = new Bike(check.getBikeId(), 1, distancia, check.getTripDuration());
						Bike checkBici = null;
						Iterator<Bike> iterBikes = bicicletas.iterator();
						boolean encontroBici = false;
						while(iterBikes.hasNext() && !encontroBici)
						{
							Bike bicicleta = iterBikes.next();
							if(bicicleta.getBikeId() == nueva.getBikeId())
							{
								checkBici = bicicleta;
								encontroBici = true;
							}
						}
						if (checkBici != null)
						{
							checkBici.addTrip();
							checkBici.addDistance(distancia);
							checkBici.addDuration(check.getTripDuration());
						}
						else
						{
							bicicletas.addAtEnd(nueva);
						}
					}
					else
					{
						//TODO Exception? o me salto el if y asumo que no hay errores en los datos cargados?
					}
				}
			}
			Bike[] bicis = new Bike[bicicletas.getSize()];
			Iterator<Bike> iterBikes2 = bicicletas.iterator();
			int i = 0;
			while (iterBikes2.hasNext() && i < bicis.length)
			{
				Bike bici = iterBikes2.next();
				bicis[i] = bici;
				i++;
			}
			OrdenadorBikesTotalTrips order = new OrdenadorBikesTotalTrips();
			order.ordenar(Ordenamientos.SHELL, true, bicis);
			bicicletas = new DoublyLinkedList<Bike>();
			i = 0;
			while (i < bicis.length)
			{
				bicicletas.addAtEnd(bicis[i]);
				i++;
			}
			return bicicletas;
		}
	}

	public IDoublyLinkedList<Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal)
	{
		DoublyLinkedList <Trip> viajes = new DoublyLinkedList<Trip>();
		Iterator<Trip> iter = listaTrips.iterator();
		while (iter.hasNext())
		{
			Trip actual = iter.next();
			if(actual.getBikeId() == bikeId && !(actual.getStartTime().isBefore(fechaInicial) || actual.getStopTime().isAfter(fechaFinal)) )
			{
				viajes.addAtEnd(actual);
			}
		}
		Trip[] trips = new Trip[viajes.getSize()];
		iter = viajes.iterator();
		int i = 0;
		while (iter.hasNext() && i < trips.length)
		{
			Trip add = iter.next();
			trips[i] = add;
			i++;
		}
		Ordenador order = new Ordenador();
		order.ordenar(Ordenamientos.SHELL, true, trips);
		viajes = new DoublyLinkedList<Trip>();
		i = 0;
		while (i < trips.length)
		{
			viajes.addAtEnd(trips[i]);
			i++;
		}
		return viajes;
	}

	public IDoublyLinkedList<Trip> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		DoublyLinkedList <Trip> viajes = new DoublyLinkedList<Trip>();
		Iterator<Trip> iter = listaTrips.iterator();
		while (iter.hasNext())
		{
			Trip actual = iter.next();
			if(actual.getEndStationId() ==  endStationId && !(actual.getStartTime().isBefore(fechaInicial) || actual.getStopTime().isAfter(fechaFinal)) )
			{
				viajes.addAtEnd(actual);
			}
		}
		Trip[] trips = new Trip[viajes.getSize()];
		iter = viajes.iterator();
		int i = 0;
		while (iter.hasNext() && i < trips.length)
		{
			Trip add = iter.next();
			trips[i] = add;
			i++;
		}
		OrdenadorTripsStopTime order = new OrdenadorTripsStopTime();
		order.ordenar(Ordenamientos.SHELL, true, trips);
		viajes = new DoublyLinkedList<Trip>();
		i = 0;
		while (i < trips.length)
		{
			viajes.addAtEnd(trips[i]);
			i++;
		}
		return viajes;
	}

	public IQueue<Station> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo) 
	{
		Queue<Station> cola = new Queue<Station>();
		Iterator<Station> iter = listaStations.iterator();
		while(iter.hasNext())
		{
			Station estacion = iter.next();
			if(estacion.getStartDate().isBefore(fechaComienzo))
			{

			}
			else
			{
				cola.enqueue(estacion);
			}
		}
		//TODO CHECK la ordeno antes de retornarla? o asumo que ya est� ordenada?
		//En ninguna parte dice que toca ordenarla (?)
		//TODO DO ? ordenar estaciones. Fecha?
		return cola;
	}

	public IDoublyLinkedList<Bike> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
	{
		DoublyLinkedList<Bike> bicicletas = new DoublyLinkedList<Bike>();
		Iterator<Trip> iterTrips = listaTrips.iterator();
		while (iterTrips.hasNext())
		{
			Trip check = iterTrips.next();
			if(check.getStartTime().isBefore(fechaInicial) || check.getStopTime().isAfter(fechaFinal))
			{

			}
			else
			{
				Station desde = null;
				Station hasta = null;
				boolean encontroDesde = false;
				boolean encontroHasta = false;
				Iterator<Station> iterStations = listaStations.iterator();
				while (iterStations.hasNext() && (!encontroDesde || !encontroHasta) )
				{
					Station checkStation = iterStations.next();
					if (check.getStartStationId() == checkStation.getStationId())
					{
						desde = checkStation;
						encontroDesde = true;
					}
					//TODO CHECK pondr�a un else if pero que tal que est� mal y sea la misma? dunno
					if (check.getEndStationId() == checkStation.getStationId())
					{
						hasta = checkStation;
						encontroHasta = true;
					}
				}
				if(desde != null || hasta != null)
				{
					int distancia = (int) Harvesine.distance(desde.getLatitude(), desde.getLongitude(), hasta.getLatitude(), hasta.getLongitude());
					Bike nueva = new Bike(check.getBikeId(), 1, distancia, check.getTripDuration());
					Bike checkBici = null;
					Iterator<Bike> iterBikes = bicicletas.iterator();
					boolean encontroBici = false;
					while(iterBikes.hasNext() && !encontroBici)
					{
						Bike bicicleta = iterBikes.next();
						if(bicicleta.getBikeId() == nueva.getBikeId())
						{
							checkBici = bicicleta;
							encontroBici = true;
						}
					}
					if (checkBici != null)
					{
						checkBici.addTrip();
						checkBici.addDistance(distancia);
						checkBici.addDuration(check.getTripDuration());
					}
					else
					{
						bicicletas.addAtEnd(nueva);
					}
				}
				else
				{
					//TODO Exception? o me salto el if y asumo que no hay errores en los datos cargados?
				}
			}
		}
		Bike[] bicis = new Bike[bicicletas.getSize()];
		Iterator<Bike> iterBikes2 = bicicletas.iterator();
		int i = 0;
		while (iterBikes2.hasNext() && i < bicis.length)
		{
			Bike bici = iterBikes2.next();
			bicis[i] = bici;
			i++;
		}
		OrdenadorBikes order = new OrdenadorBikes();
		order.ordenar(Ordenamientos.SHELL, false, bicis);
		bicicletas = new DoublyLinkedList<Bike>();
		i = 0;
		while (i < bicis.length)
		{
			bicicletas.addAtStart(bicis[i]);
			i++;
		}
		return bicicletas;
	}

	public IDoublyLinkedList<Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero) 
	{
		DoublyLinkedList <Trip> viajes = new DoublyLinkedList<Trip>();
		Iterator<Trip> iter = listaTrips.iterator();
		while (iter.hasNext())
		{
			Trip check = iter.next();
			if(check.getBikeId() == bikeId && check.getTripDuration() < tiempoMaximo && check.getGender().equalsIgnoreCase(genero))
			{
				viajes.addAtEnd(check);
			}
		}
		Trip[] trips = new Trip[viajes.getSize()];
		iter = viajes.iterator();
		int i = 0;
		while (iter.hasNext() && i < trips.length)
		{
			Trip add = iter.next();
			trips[i] = add;
			i++;
		}
		Ordenador order = new Ordenador();
		order.ordenar(Ordenamientos.SHELL, true, trips);
		viajes = new DoublyLinkedList<Trip>();
		i = 0;
		while (i < trips.length)
		{
			viajes.addAtEnd(trips[i]);
			i++;
		}
		return viajes;
	}

	public IDoublyLinkedList<Trip> B4ViajesPorEstacionInicial(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		DoublyLinkedList <Trip> viajes = new DoublyLinkedList<Trip>();
		Iterator<Trip> iter = listaTrips.iterator();
		while (iter.hasNext())
		{
			Trip check = iter.next();
			if(check.getStartTime().isBefore(fechaInicial) || check.getStopTime().isAfter(fechaFinal))
			{

			}
			else if (check.getStartStationId() == startStationId)
			{
				viajes.addAtEnd(check);
			}
		}
		Trip[] trips = new Trip[viajes.getSize()];
		iter = viajes.iterator();
		int i = 0;
		while (iter.hasNext() && i < trips.length)
		{
			Trip add = iter.next();
			trips[i] = add;
			i++;
		}
		OrdenadorTripsFechas order = new OrdenadorTripsFechas();
		order.ordenar(Ordenamientos.SHELL, true, trips);
		viajes = new DoublyLinkedList<Trip>();
		i = 0;
		while (i < trips.length)
		{
			viajes.addAtEnd(trips[i]);
			i++;
		}
		return viajes;
	}

	public void C1cargar(String rutaTrips, String rutaStations) {
		//TODO CHECK Falta archivo JSON, revisar que se carguen todos los datos posibles. No olvidar quitar contadores.
		try {
			loadStations(rutaStations);
			loadTrips(rutaTrips);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public IQueue<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Iterator<Trip> iter = listaTrips.iterator();
		DoublyLinkedList<Trip> viajes = new DoublyLinkedList<Trip>();
		Stack<Trip> stack = new Stack<Trip>();
		Queue<Trip> queue = new Queue<Trip>();
		while( iter.hasNext() )
		{
			Trip actual = iter.next();
			if (actual.getStartTime().isBefore(fechaInicial) || actual.getStopTime().isAfter(fechaFinal))
			{
				
			}
			else if (actual.getBikeId() == bikeId)
			{
				viajes.addAtStart(actual);
			}
		}
		Trip[] trips = new Trip[viajes.getSize()];
		iter = viajes.iterator();
		int i = 0;
		while (iter.hasNext() && i < trips.length)
		{
			Trip add = iter.next();
			trips[i] = add;
			i++;
		}
		OrdenadorTripsFechas order = new OrdenadorTripsFechas();
		order.ordenar(Ordenamientos.SHELL, true, trips);
		viajes = new DoublyLinkedList<Trip>();
		i = 0;
		while (i < trips.length)
		{
			viajes.addAtEnd(trips[i]);
			i++;
		}
		iter = viajes.iterator();
		while(iter.hasNext())
		{
			Trip actual = iter.next();
			if (stack.isEmpty())
			{
				stack.push(actual);
			}
			else
			{
				Trip anterior = stack.pop();
				if(anterior.getEndStationId() == actual.getStartStationId())
				{
					stack.push(anterior);
					stack.push(actual);
				}
				else
				{
					queue.enqueue(anterior);
					queue.enqueue(actual);
				}
			}
		}
		while(!stack.isEmpty())
		{
			stack.pop();
		}
		if(queue.isEmpty())
		{
			System.out.println("Los viajes de la bicicleta en el periodo de tiempo dado son consistentes");
		}
		return queue;
	}

	public IDoublyLinkedList<Bike> C3BicicletasMasUsadas(int topBicicletas) {
		DoublyLinkedList<Bike> bicicletas = new DoublyLinkedList<Bike>();
		Iterator<Trip> iterTrips = listaTrips.iterator();
		while (iterTrips.hasNext())
		{
			Trip check = iterTrips.next();
			Station desde = null;
			Station hasta = null;
			boolean encontroDesde = false;
			boolean encontroHasta = false;
			Iterator<Station> iterStations = listaStations.iterator();
			while (iterStations.hasNext() && (!encontroDesde || !encontroHasta) )
			{
				Station checkStation = iterStations.next();
				if (check.getStartStationId() == checkStation.getStationId())
				{
					desde = checkStation;
					encontroDesde = true;
				}
				//TODO CHECK pondr�a un else if pero que tal que est� mal y sea la misma? dunno
				if (check.getEndStationId() == checkStation.getStationId())
				{
					hasta = checkStation;
					encontroHasta = true;
				}
			}
			if(desde != null || hasta != null)
			{
				int distancia = (int) Harvesine.distance(desde.getLatitude(), desde.getLongitude(), hasta.getLatitude(), hasta.getLongitude());
				Bike nueva = new Bike(check.getBikeId(), 1, distancia, check.getTripDuration());
				Bike checkBici = null;
				Iterator<Bike> iterBikes = bicicletas.iterator();
				boolean encontroBici = false;
				while(iterBikes.hasNext() && !encontroBici)
				{
					Bike bicicleta = iterBikes.next();
					if(bicicleta.getBikeId() == nueva.getBikeId())
					{
						checkBici = bicicleta;
						encontroBici = true;
					}
				}
				if (checkBici != null)
				{
					checkBici.addTrip();
					checkBici.addDistance(distancia);
					checkBici.addDuration(check.getTripDuration());
				}
				else
				{
					bicicletas.addAtEnd(nueva);
				}
			}
		}
		Bike[] bicis = new Bike[bicicletas.getSize()];
		Iterator<Bike> iterBikes2 = bicicletas.iterator();
		int i = 0;
		while (iterBikes2.hasNext() && i < bicis.length)
		{
			Bike bici = iterBikes2.next();
			bicis[i] = bici;
			i++;
		}
		OrdenadorBikesDuration order = new OrdenadorBikesDuration();
		order.ordenar(Ordenamientos.SHELL, true, bicis);
		bicicletas = new DoublyLinkedList<Bike>();
		i = 0;
		while (i < topBicicletas)
		{
			bicicletas.addAtEnd(bicis[i]);
			i++;
		}
		return bicicletas;
	}
	
	public IDoublyLinkedList<Trip> C4ViajesEstacion(int stationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Iterator<Trip> iter = listaTrips.iterator();
		DoublyLinkedList<Trip> viajes = new DoublyLinkedList<Trip>();
		while (iter.hasNext())
		{
			Trip actual = iter.next();
			if (actual.getStartTime().isBefore(fechaInicial) || actual.getStopTime().isAfter(fechaFinal))
			{
				
			}
			else if (actual.getStartStationId() == stationId ||actual.getEndStationId() == stationId)
			{
				viajes.addAtEnd(actual);
			}
		}
		Trip[] trips = new Trip[viajes.getSize()];
		iter = viajes.iterator();
		int i = 0;
		while (iter.hasNext() && i < trips.length)
		{
			Trip add = iter.next();
			trips[i] = add;
			i++;
		}
		OrdenadorTripsFechas order = new OrdenadorTripsFechas();
		order.ordenar(Ordenamientos.SHELL, true, trips);
		viajes = new DoublyLinkedList<Trip>();
		i = 0;
		while (i < trips.length)
		{
			viajes.addAtEnd(trips[i]);
			i++;
		}
		return viajes;
	}

	//Metodos utiles
	public void loadStations (String rutaStations) throws FileNotFoundException, IOException
	{
		CSVReader reader = new CSVReader(new FileReader(rutaStations));
		String [] nextLine;
		nextLine = reader.readNext();

		while ((nextLine = reader.readNext()) != null ) 
		{
			LocalDateTime ldt6 = convertirFecha_Hora_LDT(nextLine[6]);

			Station nueva = new Station(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2], Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Integer.parseInt(nextLine[5]), ldt6);
			//agreagar lista
			listaStations.addAtEnd(nueva);
		}
		reader.close();
	}

	public void loadTrips (String rutaTrips) throws FileNotFoundException, IOException 
	{
		CSVReader reader = new CSVReader(new FileReader(rutaTrips));
		String [] nextLine;
		nextLine = reader.readNext();

		while ((nextLine = reader.readNext()) != null ) 
		{
			LocalDateTime ldt1 = convertirFecha_Hora_LDT(nextLine[1]);
			LocalDateTime ldt2 = convertirFecha_Hora_LDT(nextLine[2]);


			Trip nuevo = new Trip( Integer.parseInt(nextLine[0]), ldt1, ldt2, Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), nextLine[8], nextLine[10]);
			//agregar lista
			listaTrips.addAtEnd(nuevo);
		}
		reader.close();
	} 

	private static LocalDateTime convertirFecha_Hora_LDT(String linea)
	{
		String[] splitLine6 = linea.split(" ");
		String[] datosFecha = splitLine6[0].split("/");
		String[] datosHora = splitLine6[1].split(":");
		int agno = Integer.parseInt(datosFecha[2]);
		int dia = Integer.parseInt(datosFecha[1]);
		int mes = Integer.parseInt(datosFecha[0]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 0;
		if( datosHora.length > 2 ) segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}
	
	public int tripsSize()
	{
		return listaTrips.getSize();
	}
	
	public int stationsSize()
	{
		return listaStations.getSize();
	}
}
