package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T>{

	private DoublyLinkedList<T> stack;
	
	private int numELementos;
	
	public Stack() 
	{
		stack = new DoublyLinkedList<T>();
		numELementos=0;
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		return stack.iterator();
	}

	@Override
	public boolean isEmpty() 
	{
		return numELementos == 0;
	}

	@Override
	public int size() 
	{
		return numELementos;
	}

	@Override
	public void push(T t) 
	{
		stack.addAtEnd(t);
		numELementos++;
	}

	@Override
	public T pop() 
	{
		Node<T> tail = stack.darUltimo();
		stack.deleteAtEnd();
		numELementos--;
		return tail.getElement();
	}

}
