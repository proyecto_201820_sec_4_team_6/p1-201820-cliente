package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T>{

	private int numElementos;

	private Node<T> primero;

	private Node<T> ultimo;

	//usado solo para no crear nuevas variables
	private Node<T> actual;

	public DoublyLinkedList() 
	{
		numElementos = 0;
		primero = null;
		ultimo = null;
		actual = null;
	}

	//------------------------------------------
	/**
	 * Metodos utiles //---------------------------
	 */
	//------------------------------------------


	public boolean isEmpty()
	{
		return numElementos == 0;
	}

	public Node<T> searchNode( T e )
	{
		if( !isEmpty() )
		{
			actual = primero;
			while( (actual = actual.next()) != null )
			{
				if( actual.getElement().equals(e) )
				{
					return actual;
				}
			}
		}
		return null;
	}

	//------------------------------------------
	/**
	 * Metodos implementados //---------------------------
	 */
	//------------------------------------------

	public void addAtStart(T elemento)
	{
		actual = new Node<T>(elemento);
		if(isEmpty())
		{
			ultimo = actual;
		}
		else
		{
			actual.setNext(primero);
			primero.setPrevious(actual);
		}
		primero = actual;
		numElementos++;
	}

	public void addAtEnd(T elemento)
	{
		actual = new Node<T>(elemento);
		if(isEmpty())
		{
			primero = actual;
		}
		else
		{
			actual.setPrevious(ultimo);
			ultimo.setNext(actual);
		}
		ultimo = actual;
		numElementos++;
	}

	/**
	 * Retorna el elemento en una posici�n dada.
	 * @param posicion , 0<= posicion <= numElementos
	 * @return elemento en la posici�n dada.
	 */
	public T darElemento(int posicion)
	{
		int i = 0;
		T buscado = null;
		actual = primero;
		while (actual != null && i < posicion && i < numElementos)
		{
			actual = actual.next();
			i++;
		}
		if(isEmpty())
		{

		}
		else
		{
			buscado = actual.getElement();
		}
		return buscado;
	}

	public Node<T> darNodo( int posicion )
	{
		int i = 0;
		Node<T> buscado = null;
		actual = primero;
		while (actual != null && i < posicion && i < numElementos)
		{
			actual = actual.next();
			i++;
		}
		if(isEmpty())
		{

		}
		else
		{
			buscado = actual;
		}
		return buscado;
	}

	public Iterator<T> iterator() 
	{
		Iterator<T> iter = new IteradorLista();
		return iter;
	}

	private class IteradorLista implements Iterator<T>
	{
		private Node<T> actual = primero;

		public boolean hasNext()
		{ 
			return actual != null; 
		}

		public void remove() 
		{ 
			throw new UnsupportedOperationException();
		}

		public T next()
		{
			if(!hasNext())
			{
				throw new NoSuchElementException();
			}
			T elemento = actual.getElement();
			actual = actual.next();
			return elemento;
		}
	} 

	@Override
	public int getSize() {
		return numElementos;
	}

	@Override
	public void deleteAtEnd() {
		// TODO CHECK
		if( isEmpty() ) 
			System.out.println("No hay elementos");
		else if( numElementos == 1 ){
			primero = null; 
			ultimo = null;
		}
		else{
			actual = ultimo.previous();
			actual.setNext(null);
			ultimo = actual;
		}
		numElementos--;
	}

	@Override
	public void deleteAtBeg() {
		// TODO CHECK
		if( isEmpty() ) 
			System.out.println("No hay elementos");
		else if( numElementos == 1 ){
			primero = null; 
			ultimo = null;
		}
		else{
			actual = primero.next();
			actual.setPrevious(null);
			primero = actual;
		}
		numElementos--;
	}

	@Override
	public void deleteByElement(T e) {
		// TODO CHECK
		actual = searchNode(e);
		if( actual != null ){
			actual.previous().setNext(actual.next());
			actual.next().setPrevious(actual.previous());
			numElementos--;
		}
		else{
			System.out.println("Nodo buscado era null");
		}
	}

	/**
	 * Elimina un elemento dado de la lista.
	 * pre: la lista no est� vac�a
	 * @param elemento
	 * @return true si el elemento fue eliminado, false si no.
	 */
	public boolean remove(T elemento)
	{
		actual = primero;
		int z = 3;
		z++;
		boolean respuesta = false;
		if(isEmpty())
		{
			return respuesta;
		}
		else if( numElementos == 1 )
		{
			primero = null;
			ultimo = null;
			numElementos--;
			respuesta = true;
		}
		else if( primero.getElement().equals(elemento) )
		{
			actual = primero.next();
			actual.setPrevious(null);
			primero = actual;
			numElementos--;
			respuesta = true;
		}
		else if( ultimo.getElement().equals(elemento) )
		{
			actual = ultimo.previous();
			actual.setNext(null);
			ultimo = actual;
			numElementos--;
			respuesta = true;
		}
		else
		{
			while (actual != null && !actual.getElement().equals(elemento) )
			{
				actual = actual.next();
			}
			if (actual == null)
			{
				return false;
			}
			else
			{
				Node<T> tempS = actual.next();
				Node<T> tempA = actual.previous();
				tempA.setNext(tempS);
				tempS.setPrevious(tempA);
				respuesta = true;
				numElementos--;
			}
		}
		return respuesta;
	}

	public Node<T> darUltimo(){
		return ultimo;
	}

	public Node<T> darPrimero(){
		return primero;
	}


}
