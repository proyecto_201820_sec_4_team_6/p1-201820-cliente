package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T>{

	private int numElementos;
	
	private DoublyLinkedList<T> queue;
	
	public Queue() 
	{
		queue = new DoublyLinkedList<T>();
		numElementos = 0;
	} 
	
	@Override
	public Iterator<T> iterator() 
	{
		return queue.iterator();
	}

	@Override
	public boolean isEmpty() 
	{
		return numElementos == 0;
	}

	@Override
	public int size() 
	{
		return numElementos;
	}

	@Override
	public void enqueue(T t) 
	{
		queue.addAtEnd(t);
		numElementos++;
	}
	
	@Override
	public T dequeue() 
	{
		Node<T> head = queue.darPrimero();
		queue.deleteAtBeg();
		numElementos--;
		return head.getElement();
	}

}
