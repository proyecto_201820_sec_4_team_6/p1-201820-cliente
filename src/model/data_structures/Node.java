package model.data_structures;

public class Node<T> {
	
	private T element;
	
	private Node<T> next;
	
	private Node<T> previous;
	
	public Node( T e){
		element = e;
	}
	
	public Node<T> next(){
		return next;
	}
	
	public void setNext( Node<T> n ){
		next = n;
	}
	
	public Node<T> previous(){
		return previous;
	}
	
	public void setPrevious( Node<T> n ){
		previous = n;
	}
	
	public T getElement(){
		return element;
	}
	
	public void setElement( T e ){
		element = e;
	}
}
