package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Test;

import model.vo.VOTrip;

public class OrdenadorTest {

	private Ordenador order;
	@Test
	public void testShell()
	{
		order = new Ordenador();
		VOTrip viaje1 = new VOTrip(1, "7", null, 4, 3, 4, "h", 5, "j", "k", "l", "7");
		VOTrip viaje2 = new VOTrip(2, "6", null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		VOTrip viaje3 = new VOTrip(3, "6", null, 3, 3, 4, "h", 5, "j", "k", "l", "6");
		VOTrip viaje4 = new VOTrip(4, "8", null, 2, 3, 4, "h", 5, "j", "k", "l", "8");
		VOTrip viaje5 = new VOTrip(4, "8", null, 1, 3, 4, "h", 5, "j", "k", "l", "8");
		VOTrip viaje6 = new VOTrip(4, "8", null, 7, 3, 4, "h", 5, "j", "k", "l", "8");
		VOTrip[] listaViajes = new VOTrip[6];
		listaViajes[0] = viaje3;
		listaViajes[1] = viaje2;
		listaViajes[2] = viaje1;
		listaViajes[3] = viaje4;
		listaViajes[4] = viaje6;
		listaViajes[5] = viaje5;
		order.ordenar(Ordenamientos.SHELL, true, listaViajes);
		assertTrue("La lista no qued� ordenada por ShellSort", order.isSorted(listaViajes));
	}
	@Test
	public void testQuick()
	{
		order = new Ordenador();
		VOTrip viaje1 = new VOTrip(1, "7", null, 4, 3, 4, "h", 5, "j", "k", "l", "7");
		VOTrip viaje2 = new VOTrip(2, "6", null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		VOTrip viaje3 = new VOTrip(3, "6", null, 3, 3, 4, "h", 5, "j", "k", "l", "6");
		VOTrip viaje4 = new VOTrip(4, "8", null, 2, 3, 4, "h", 5, "j", "k", "l", "8");
		VOTrip viaje5 = new VOTrip(4, "8", null, 1, 3, 4, "h", 5, "j", "k", "l", "8");
		VOTrip viaje6 = new VOTrip(4, "8", null, 7, 3, 4, "h", 5, "j", "k", "l", "8");
		VOTrip[] listaViajes = new VOTrip[6];
		listaViajes[0] = viaje3;
		listaViajes[1] = viaje2;
		listaViajes[2] = viaje1;
		listaViajes[3] = viaje4;
		listaViajes[4] = viaje6;
		listaViajes[5] = viaje5;
		order.ordenar(Ordenamientos.QUICK, true, listaViajes);
		assertTrue("La lista no qued� ordenada por QuickSort", order.isSorted(listaViajes));
	}
	@Test
	public void testMerge()
	{
		order = new Ordenador();
		VOTrip viaje1 = new VOTrip(1, "7", null, 4, 3, 4, "h", 5, "j", "k", "l", "7");
		VOTrip viaje2 = new VOTrip(2, "6", null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		VOTrip viaje3 = new VOTrip(3, "6", null, 3, 3, 4, "h", 5, "j", "k", "l", "6");
		VOTrip viaje4 = new VOTrip(4, "8", null, 2, 3, 4, "h", 5, "j", "k", "l", "8");
		VOTrip viaje5 = new VOTrip(4, "8", null, 1, 3, 4, "h", 5, "j", "k", "l", "8");
		VOTrip viaje6 = new VOTrip(4, "8", null, 7, 3, 4, "h", 5, "j", "k", "l", "8");
		VOTrip[] listaViajes = new VOTrip[6];
		listaViajes[0] = viaje3;
		listaViajes[1] = viaje2;
		listaViajes[2] = viaje1;
		listaViajes[3] = viaje4;
		listaViajes[4] = viaje6;
		listaViajes[5] = viaje5;
		order.ordenar(Ordenamientos.MERGE, true, listaViajes);
		assertTrue("La lista no qued� ordenada por ShellSort", order.isSorted(listaViajes));
	}
}
