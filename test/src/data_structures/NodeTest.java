package model.data_structures;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.vo.Trip;

public class NodeTest {

	@Test
	public void test()
	{
		Trip viaje1 = new Trip(1, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Trip viaje2 = new Trip(2, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Trip viaje3 = new Trip(3, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Node<Trip> actual = new Node<Trip>(viaje1);
		Node<Trip> anterior = new Node<Trip>(viaje2);
		Node<Trip> siguiente = new Node<Trip>(viaje3);
		actual.setPrevious(anterior);
		actual.setNext(siguiente);
		assertEquals( "El n�mero asignado al nodo actual no corresponde.", 1, actual.getElement().darId());
		assertEquals( "El nodo anterior no corresponde.", 2, actual.previous().getElement().darId());
		assertEquals( "El nodo siguiente no corresponde.", 3, actual.next().getElement().darId());
		actual.setElement(viaje3);
		assertEquals( "El cambio de elemento fall�.", 3, actual.getElement().darId());
	}
}
