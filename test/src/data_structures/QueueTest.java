package model.data_structures;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import model.vo.Trip;

public class QueueTest 
{
	@Test
	public void test()
	{
		Trip viaje1 = new Trip(1, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Trip viaje2 = new Trip(2, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Trip viaje3 = new Trip(3, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Queue<Trip> queueViajes = new Queue<>();
		assertEquals( "size() est� fallando", 0, queueViajes.size());
		assertTrue( "isEmpty() est� fallando", queueViajes.isEmpty());
		queueViajes.enqueue(viaje1);
		assertEquals( "El primero no se asign� correctamente", 1, queueViajes.size());
		queueViajes.enqueue(viaje2);
		assertEquals( "El segundo no fue a�adido", 2, queueViajes.size());
		Iterator<Trip> iter = queueViajes.iterator();
		assertTrue( "El iterador est� fallando", iter.hasNext());
		assertEquals( "El segundo fue a�adido en la posici�n equivocada.", 1, iter.next().darId());
		queueViajes.enqueue(viaje3);
		assertEquals( "Se elimin� un elemento equivocado", viaje1, queueViajes.dequeue());
	}
}
