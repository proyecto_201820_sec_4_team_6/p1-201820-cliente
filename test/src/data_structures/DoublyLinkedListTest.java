package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Test;

import model.vo.Trip;

public class DoublyLinkedListTest {

	@Test
	public void test()
	{
		Trip viaje1 = new Trip(1, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Trip viaje2 = new Trip(2, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Trip viaje3 = new Trip(3, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Trip viaje4 = new Trip(4, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		DoublyLinkedList<Trip> listaViajes = new DoublyLinkedList<>();
		assertEquals( "getSize() est� fallando", 0, listaViajes.getSize());
		listaViajes.addAtStart(viaje1);
		assertEquals( "El primero no se asign� correctamente", 1, listaViajes.getSize());
		listaViajes.addAtStart(viaje2);
		assertEquals( "El segundo no fue a�adido", 2, listaViajes.getSize());
		assertEquals( "El segundo fue a�adido en la posici�n equivocada.", 2, listaViajes.darElemento(0).darId());
		assertEquals( "El m�todo darElemento(int posicion) est� fallando.", 1, listaViajes.darElemento(1).darId());
		assertTrue( "No se elimin� el elemento.", listaViajes.remove(viaje1));
		assertNull( "No se elimin� el elemento.", listaViajes.darNodo(1));
		listaViajes.addAtStart(viaje1);
		listaViajes.addAtStart(viaje3);
		assertFalse( "Se 'borr�' un elemento que no estaba en la lista.", listaViajes.remove(viaje4));
		listaViajes.remove(viaje1);
		assertEquals( "No se reasignaron los elementos tras eliminar uno.", viaje2, listaViajes.darElemento(1));
	}
}
