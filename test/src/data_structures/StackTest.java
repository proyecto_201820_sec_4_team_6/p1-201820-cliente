package model.data_structures;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import model.vo.Trip;

public class StackTest 
{
	@Test
	public void test()
	{
		Trip viaje1 = new Trip(1, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Trip viaje2 = new Trip(2, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Trip viaje3 = new Trip(3, null, null, 2, 3, 4, "h", 5, "j", "k", "l", "6");
		Stack<Trip> stackViajes = new Stack<>();
		assertEquals( "size() est� fallando", 0, stackViajes.size());
		assertTrue( "isEmpty() est� fallando", stackViajes.isEmpty());
		stackViajes.push(viaje1);
		assertEquals( "El primero no se asign� correctamente", 1, stackViajes.size());
		stackViajes.push(viaje2);
		assertEquals( "El segundo no fue a�adido", 2, stackViajes.size());
		Iterator<Trip> iter = stackViajes.iterator();
		assertTrue( "El iterador est� fallando", iter.hasNext());
		assertEquals( "El segundo fue a�adido en la posici�n equivocada.", 1, iter.next().darId());
		stackViajes.push(viaje3);
		assertEquals( "Se elimin� un elemento equivocado", viaje3, stackViajes.pop());
	}
}
